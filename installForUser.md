# INSTALL VALHEIM PLUS FOR USER
### :warning: This server use Valheim BepInEx / Valheim PLUS

### 1. **Téléchargez l'archive du mod client**

https://github.com/valheimPlus/ValheimPlus/releases

Sur la page récupérer la dernière version puis **WindowsClient.zip**

### 2. **Extraire les fichiers de l'archive dans votre jeu**

Il suffit d'extraire tous les fichiers dans le dossier de votre jeu:

exemple: ```C:\Program Files (x86)\Steam\steamapps\common\Valheim```
