# Server Valheim by Soxzz
### :warning: This server use Valheim BepInEx / Valheim PLUS
***Penser à installer le mod côté client pour rejoindre le serveur***

![INSTALLATION CLIENT](./installForUser.md)

# PATCH NOTE VERSION 1

## MODS

**Liste des mods activés**
- **Valheim PLUS**

## CONFIG

**Configuration des mods**

### Valheim PLUS

- **Advanced Building Mode :**

    ![](https://i.imgur.com/ddQCzPy.mp4)

    |    RACCOURCI        |         ACTION                 |
    |---------------------|--------------------------------|
    |F1                   |       Active le mode avancée   |
    |F3                   | Désactive le mode avancée      |
    |Touches fléchées     | Bouge l'objet sur l'axe X / Y  |
    |Ctrl & Haut/Bas      | Bouge l'objet sur l'axe Z      |
    |Molette              | Rotation sur l'axe Y           |
    |Ctrl & Molette       | Rotation sur l'axe X           |
    |Alt Gauche & Molette | Rotation sur l'axe Z           |
    |Numpad +/-           | Augmenter / Réduire la vitesse |

- **Ruche :**
    - Modification du miel produit par ruche de **4** à **10**
- **Construction :**
    - Pas de dégats de la pluie sur les batiments
    - Distance max de placement en mode construction modifier de **5** à **10**
    - Augmentation de l'intégrité des structures par matériaux :
        |    MATERIAUX | POURCENTAGE |
        |--------------|-------------|
        |Bois          |   +15%      |
        |Pierre        | +30%        |
        |Fer           | +50%        |
        |Bois Solide   | +30%        |
- **Objets :**
    - Téléportation possible avec tous les objets plus de restrictions
    - Réduction du poids de base des objets de **50%**
    - Augmentation du nombre d'objets par stack de **100%**
- **Férmenteur :**
    - Réduction de la durée de fermentation de **2400** à **1200**
    - Augmentation du nombre d'hydromel produit de **6** à **10**
- **Torche & Feu de camp :**
    - Plus besoin de recharger les torches & feux de camps
- **Nourriture :**
    - Augmentation de la durée de la nourriture par **50%**
- **Fonderie :**
    - Augmentation du nombre de minerai possible de **10** à **25**
    - Augmentation du nombre de charbon possible de **20** à **50**
    - Réduction du temps de production de **30** à **15**
- **Difficulté :**
    - Activation du mod
    - Distance de détection des joueurs **200**
    - Multiplicateur de difficulté par joueur **0.4**
- **Dodge :**
    - Activation du mod
        |    RACCOURCI        |         ACTION      |
        |---------------------|---------------------|
        |F9                   |  Esquive en avant   |
        |F10                  | Esquive en arrière  |
- **Hud :**
    - Montre les items requis en mode build
    - Notification d'expérience gagné en haut a gauche de l'écran
- **Four à charbon :**
    - Augmentation du nombre de bois de **20** à **100**
    - Réduction du temps de production de **30** à **10**
- **Map :**
    - Activation du partage de la map
    - Augmentation du rayon d'exploration de **100** à **150**
    - Activation automatique du mode share quand vous vous connectez 
    - Blocage pour empêcher d'enlever le mode share
    - Disparition de votre dernière mort quand vous récupérer votre stuff
- **Joueur :**
    - Augmentation du poids portable de base de **350** à **500**
    - Augmentation du buff de Megingjord's de **150** à **300**
    - Désactivation du shake de la caméra quand vous prenez un coup
- **Server :**
    - Autosave modifier de **1200** ***(24h en jeu)*** à **600** ***(12h en jeu)***
- **Stamina :**
    - Réduction des actions : **dodge**,**jump**,**run** ,**sneak** de **10** à **5** pts de stamina
- **Table de travail :**
    - Augmentation du rayon de construction de **20** à **60**
    - Plus besoin de toit pour utiliser les workbench ;)
- **Temps :**
    - Accélération de la nuit par **25%**
- **Chariot :**
    - Augmentation de la masse de base de **20** à **30**
    - Rédution de la masse des objets à l'intérieur par **25%**
